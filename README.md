### Example "Generic" IOC for Keysight B2985

This IOC expects to run in an environment where the following environment variables are set

    - IOC_SYS=
    - IOC_DEV=
    - IOC_PORT=
    - IOC_IP=

Additionally, a folder should be available at `/opt/epics/autosave/$(IOC_SYS)_$(IOC_DEV)`. It's intended that this directory is mounted as a volume by something like docker-compose.

