#!../../bin/linux-x86_64/SISSY_TEST_KTH_00

#- You may have to change SISSY_TEST_KTH_00 to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

#epicsEnvSet("SYS", "SISSY")
#epicsEnvSet("DEV", "Keysight")

# For use as a generic docker iocBoot

epicsEnvSet("SYS", "$(IOC_SYS)")
epicsEnvSet("DEV", "$(IOC_DEV)")

epicsEnvSet("STREAM_PROTOCOL_PATH","$(KEITHLEY)/db")

epicsEnvSet("ENGINEER","Will Smith")
epicsEnvSet("LOCATION", "TEST")

## Register all support components
dbLoadDatabase "dbd/SISSY_TEST_KTH_00.dbd"
SISSY_TEST_KTH_00_registerRecordDeviceDriver pdbbase

drvAsynIPPortConfigure("KeysightPort","$(IOC_IP):$(IOC_PORT)")
asynOctetSetInputEos("KeysightPort", -1, "\n")
asynOctetSetOutputEos("KeysightPort", -1, "\n")



set_requestfile_path("$(TOP)/SISSY_TEST_KTH_00App/Db", "")

set_savefile_path("/opt/epics/autosave/$(IOC_SYS)_$(IOC_DEV)")
set_pass0_restoreFile("settings.sav","SYS=$(SYS), DEV=$(DEV)")
save_restoreSet_DatedBackupFiles(0)

## Load record instances
dbLoadRecords("db/keysight.db","SYS=$(SYS),DEV =$(DEV),PORT=KeysightPort,PROTO=KeysightB2985A.proto")
dbLoadRecords("$(ASYN)/db/asynRecord.db","P=$(SYS):,R=$(DEV):asyn,PORT=KeysightPort,ADDR=-1,IMAX=0,OMAX=0")


cd "${TOP}/iocBoot/${IOC}"
iocInit

create_monitor_set("settings.req",5, "SYS=$(SYS), DEV=$(DEV)")



